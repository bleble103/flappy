package agh.markowski.flappy;

import javazoom.jl.player.Player;

import java.io.InputStream;

public class SoundThread extends Thread {
    public SoundThread(String str){
        super(str);
    }
    public void run() {
        try {
            while(true) {
                InputStream is = new SoundLoader().load("/ts4.mp3");
                Player player = new Player(is);
                player.play();
            }
        }
        catch(Exception e){
            System.out.println("Problem with sound: "+e.getMessage());
        }
    }
}
