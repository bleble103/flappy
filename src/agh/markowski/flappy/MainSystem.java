package agh.markowski.flappy;

import javazoom.jl.player.Player;
import jline.console.ConsoleReader;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MainSystem {
    private static ConsoleReader console;
    private static void printSplash() throws IOException {
        String endCaption = 
                    "| |                                                | |\n" +
                    "| |              +----------------+                | |\n" +
                    "| |              |     Flappy!    |                | |\n" +
                    "| |              +----------------+                | |\n" +
                    "| |                                                | |\n" +
                    "| |                                                | |\n" +
                    "| |                                                | |\n" +
                    "| |      +--------------------------------+        | |\n" +
                    "| |      |        Use ~W~ to jump         |        | |\n" +
                    "| |      +--------------------------------+        | |\n" +
                    "| |                                                | |\n" +
                    "| |                                                | |\n" +
                    "| |                                                | |\n"+
                    "| |           "+(char)27+"[5mPress ~ANY KEY~ to start!"+(char)27+"[0m            | |\n"+
                    "| |                                                | |\n"+
                    "| |                            2019 DzikiLamer \u00a9   | |";
            


            Stat.clear();
            //borders
            System.out.print(' ');
            for(int x=1; x<= 48 + 4; x++) System.out.print('_');
            System.out.print('\n');
            System.out.print("|\\");
            for(int x=1; x<= 48 + 2; x++) System.out.print('_');
            System.out.print("/|\n");
            //End of borders
            console.println(endCaption);
            console.flush();
            //System.out.println(endCaption);
            //borders
            System.out.print("| |");
            for(int x=1; x<= 48; x++) System.out.print('_');
            System.out.print("| |\n");
            System.out.print("|/");
            for(int x=1; x<= 48 + 2; x++) System.out.print('_');
            System.out.print("\\|");
            //End of borders
    }

    public static void main(String[] args){
        int a = 1;
        try (ConsoleReader tryConsole = console = new ConsoleReader()) {


            SoundThread soundThread = new SoundThread("ts4");
            soundThread.start();
            Stat.console = console;
            printSplash();
            console.readCharacter();
            while(Stat.deepPlay) {
                Stat.bird = Stat.startQuant;
                Stat.quant = Stat.startQuant;
                Stat.speed = Stat.startSpeed;
                Stat.score = 0;
                int c = 0;
                MyThread myThread = new MyThread("my");
                myThread.start();
                while (Stat.play) {
                    c = console.readCharacter();
                    //System.out.println(c);
                    if (c == 119) {
                        //w
                        Stat.speed = Stat.jumpSpeed;
                        
                    }
                }
                boolean decide = true;
                while(decide){
                    if(c == 'q') {
                        Stat.deepPlay = false;
                        decide = false;
                    }
                    else if(c == 'r'){
                        Stat.play = true;
                        decide = false;
                    }
                    else{
                        c = console.readCharacter();
                    }
                }

            }
            soundThread.stop();
            Stat.clear();




        }
        catch(Exception e){
            System.out.println("Problem "+e.getMessage());
        }
    }
}
