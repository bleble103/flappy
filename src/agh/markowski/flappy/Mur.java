package agh.markowski.flappy;

public class Mur {
    public int top;
    public int bottom;
    public int quant;
    public double x;
    public boolean scored = false;
    public double toBlink = 0;

    public Mur(){

        x = 48;
        quant = 48;
        top = (int)(Math.random()*7) +2;
        bottom = 16 - top - 6;
    }
}
