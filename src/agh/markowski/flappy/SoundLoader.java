package agh.markowski.flappy;

import java.io.InputStream;

public class SoundLoader {
    public InputStream load(String name){
        return getClass().getResourceAsStream(name);
//        ClassLoader cl = getClass().getClassLoader();
//        return cl.getResourceAsStream(name);
    }
}
