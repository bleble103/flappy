package agh.markowski.flappy;

import jline.console.ConsoleReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MyThread extends Thread {
    private List<Mur> murs;
    private List<Cloud> clouds;
    private List<Cloud> cloudsToDelete;
    private boolean escape = false;
    private ConsoleReader console;

    public void printScreen() throws IOException {
        //clear
        for(int x=1; x<=48; x++){
            for(int y=1; y<= 16; y++){
                Stat.screen[x][y] = ' ';
            }
        }

        //clouds
        for(Cloud cloud : clouds){
            Stat.screen[cloud.quant][cloud.y] = cloud.cloud;
        }

        //murs
        for(Mur mur : murs){
            // if trzeba
            char wallChar = '#';
            if(mur.toBlink > 0) wallChar = '$';
            for(int i=0; i<mur.top; i++){
                Stat.screen[mur.quant][16-i] = wallChar;
            }
            for(int i=0; i<mur.bottom; i++){
                Stat.screen[mur.quant][i+1] = wallChar;
            }
        }



        //bird
        if(Stat.quant < 1 || Stat.quant > 16 || Stat.screen[Stat.birdX][Stat.quant] == '#'){
            //collision
            if(Stat.score > Stat.record) Stat.record = Stat.score;
            Stat.play = false;
            Stat.clear();
            String endCaption = 
                    "| |                                                | |\n" +
                    "| |              +----------------+                | |\n" +
                    "| |              |   "+(char)27+"[5mGame over!"+(char)27+"[0m   |                | |\n" +
                    "| |              |                |                | |\n" +
                    "| |              |   Score: "+String.format("%4d",Stat.score)+"  |                | |\n" +
                    "| |              |  Record: "+String.format("%4d",Stat.record)+"  |                | |\n" +
                    "| |              +----------------+                | |\n" +
                    "| |                                                | |\n" +
                    "| |      +------------+      +------------+        | |\n" +
                    "| |      |     ~R~    |      |     ~Q~    |        | |\n" +
                    "| |      | Play again |      |    Quit    |        | |\n" +
                    "| |      +------------+      +------------+        | |\n" +
                    "| |                                                | |\n"+
                    "| |                                                | |\n"+
                    "| |                                                | |\n"+
                    "| |                                                | |";
            
            //borders
            System.out.print(' ');
            for(int x=1; x<= 48 + 4; x++) System.out.print('_');
            System.out.print('\n');
            System.out.print("|\\");
            for(int x=1; x<= 48 + 2; x++) System.out.print('_');
            System.out.print("/|\n");
            //End of borders
            System.out.println(endCaption);
            //borders
            System.out.print("| |");
            for(int x=1; x<= 48; x++) System.out.print('_');
            System.out.print("| |\n");
            System.out.print("|/");
            for(int x=1; x<= 48 + 2; x++) {
                System.out.print('_');
            }
            System.out.print("\\|");
            //End of borders
            return;
        }
        Stat.screen[Stat.birdX][Stat.quant] = '@';



        //Printing screen
        Stat.clear();
        //borders
        console.print(" ");
        for(int x=1; x<= 48 + 4; x++) console.print("_");
        console.print("\n");
        console.print("|\\");
        for(int x=1; x<= 48 + 2; x++) console.print("_");
        console.print("/|\n");
        //End of borders
        for(int y=16; y>= 1; y--){
            console.print("| |");
            for(int x=1; x<= 48; x++){
                escape = false;
                if(Stat.screen[x][y] == '#'){
                    Stat.screen[x][y] = ' ';
                    console.print((char) 27 + "[48;5;46m");
                    escape = true;
                }
                else if(Stat.screen[x][y] == '$'){

                    Stat.screen[x][y] = ' ';
                    console.print((char) 27 + "[48;5;160m");
                    //console.print((char) 27 + "[42;1m");
                    //console.print((char)27 +"[32;1m" );
                    escape = true;
                }
                else if(Stat.screen[x][y] == '@'){
                    //console.print((char) 27 + "[48;5;226m");
                    console.print((char) 27 + "[48;5;37m");
                    console.print((char) 27 + "[38;5;226m");

                }
                else if(Stat.screen[x][y] == '~' || Stat.screen[x][y] == '-'){
                    console.print((char) 27 + "[48;5;37m");
                    console.print((char) 27 + "[38;5;15m");
                    //console.print((char)27 + "[48;5;15m");
                }
                else console.print((char) 27 + "[48;5;37m");
                console.print(Character.toString( Stat.screen[x][y] ));
                //if(escape) console.print((char)27 +"[0m" );
            }
            console.print((char)27 +"[0m" );
            console.print("| |");
            console.print("\n");
        }
        //borders
        console.print("| |");
        for(int x=1; x<= 48; x++) console.print("_");
        console.print("| |\n");
        console.print("|/");
        int auxCounter = 1;
        for(int x=1; x<= 48 + 2; x++){
            if(auxCounter == 21){
                auxCounter = 22;
                console.print(String.format("SCORE: %3d", Stat.score));
                x += 9;
                continue;
            }
            console.print("_");

            auxCounter++;
        }
        console.print("\\|");
        //End of borders

        //refresh whole frame
        console.flush();
    }

    public MyThread(String str){
        super(str);
    }
    public void run(){
        console = Stat.console;
        try {
            cloudsToDelete = new ArrayList<>();
            double nextCloud = 0.5;
            double nextWall=0.5;
            clouds = new ArrayList<>();
            murs = new ArrayList<>();


            long last = System.currentTimeMillis();
            Stat.screen[1][1] = Stat.screen[48][16] = '*';

            while (Stat.play) {
                double dt = (System.currentTimeMillis() - last)/1000.0;
                last = System.currentTimeMillis();


                //wall
                nextWall -= dt;
                if(nextWall <= 0){
                    nextWall = Math.random() + 0.5;
                    murs.add(new Mur());
                }
                Mur toDelete = null;
                for(Mur mur : murs){
                    mur.x -= dt*Stat.wallSpeed;
                    mur.quant = (int)mur.x;
                    if(mur.toBlink > 0) mur.toBlink -= dt;
                    if(mur.quant <= 0) toDelete = mur;
                    if(mur.x < Stat.birdX && !mur.scored){
                        Stat.score++;
                        mur.scored = true;
                        mur.toBlink = Stat.toBlink;
                    }
                }
                if(toDelete != null) murs.remove(toDelete);

                //cloud
                nextCloud -= dt;
                if(nextCloud <= 0){
                    nextCloud = Math.random();
                    clouds.add(new Cloud());
                }
                cloudsToDelete.clear();
                //Cloud cToDelete = null;
                for(Cloud cloud : clouds){
                    cloud.x -= dt*Stat.cloudSpeed;
                    cloud.quant = (int)cloud.x;
                    if(cloud.quant <= 0) cloudsToDelete.add(cloud);
                }
                for(Cloud cloud:cloudsToDelete){
                    clouds.remove(cloud);
                }


                //bird
                Stat.speed += Stat.gravity*dt;
                Stat.bird += Stat.speed*dt;
                Stat.quant = (int)(Stat.bird);


                printScreen();

                Thread.sleep(50);


            }




        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
