package agh.markowski.flappy;

public class Cloud {
    public char cloud = '-';
    public double x = 48;
    public int quant = 48;
    public int y = 16;

    public Cloud(){
        y = (int)(Math.random()*5) + 10;
        if(Math.random() > 0.5) cloud = '~';
    }
}
