package agh.markowski.flappy;

import jline.console.ConsoleReader;

import java.io.IOException;

public class Stat {
    public static boolean play = true;
    public static boolean deepPlay = true;
    public static int startQuant = 8;
    public static double startSpeed = -1;
    public static double bird = 8;
    public static int birdX = 10;
    public static int quant = 8;
    public static double cloudSpeed = 5;
    public static double wallSpeed = 20;
    public static double speed = -1;
    public static double jumpSpeed = 20;
    public static double gravity = -50;
    public static int score = 0;
    public static int record = 0;
    public static double toBlink = 0.1;
    public static char[][] screen;
    public static ConsoleReader console = null;


    public static void clear(){
        try {
            console.clearScreen();
            console.flush();
//            System.out.print("\033[H\033[2J");
        }
        catch(IOException e){
            System.out.println("Console clear screen problem: "+e.getMessage());
        }
    }

    static{
        screen = new char[49][17];
        for(int x=1; x<=48; x++){
            for(int y=1; y<= 16; y++){
                screen[x][y] = ' ';
            }
        }
    }
}
